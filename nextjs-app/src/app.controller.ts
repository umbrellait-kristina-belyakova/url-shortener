import { Controller, ExecutionContext, Get, Param, Redirect, Res } from '@nestjs/common';
import { AppService } from './app.service';
const baseUrl = 'http://localhost:3000';

@Controller()
export class AppController {
  constructor(private readonly AppService: AppService) {}

  @Get(':short')
  @Redirect(baseUrl, 302)
  async redirect(
    @Param('short') short,
  ) {
    const fullUrl = baseUrl + '/' + short;
    const longUrl = await this.AppService.getLongUrl(fullUrl);
    
    if (longUrl) {
      return { url: longUrl }
    }
  }
}
