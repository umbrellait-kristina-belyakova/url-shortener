import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Url } from './url.model';

@Injectable()
export class UrlService {
    constructor(@InjectModel("URL") private readonly UrlModel: Model<Url>) { }

    async createUrl(urlCode: string, longUrl: string, shortUrl: string) {
        const newUrl = new this.UrlModel({
            urlCode,
            longUrl,
            shortUrl
        });
        const result = await newUrl.save();
        return result;
    } 
    async getOneUrl(longUrl: string) {
        const url = await this.findOneUrl(longUrl);
        return {
            urlCode: url.urlCode,
            longUrl: url.longUrl,
            shortUrl: url.shortUrl
        }
    }
    private async findOneUrl(longUrl: string): Promise<any> {
        let url;
        try {
            const allUrlFound = await this.UrlModel.find({longUrl});
            url = {
                urlCode: allUrlFound[0].urlCode,
                longUrl: allUrlFound[0].longUrl,
                shortUrl: allUrlFound[0].shortUrl,
            }
        } catch (error) {
            // throw new NotFoundException('Could not find product.');
            return false;
        }
        if (!url) {
            return false;
        }
        return url;
    }
}