import { IsString, IsDefined, IsUrl, IsOptional } from 'class-validator';

export class CreateUrlDto {
  @IsString()
  @IsDefined()
  @IsUrl(undefined, { message: 'not valid url' })
  longUrl: string;

  @IsOptional()
    @IsString()
    desired_URL: string;
}
