import * as mongoose from 'mongoose'

export const urlSchema = new mongoose.Schema({
    urlCode: { type: String, required: true },
    longUrl: { type: String, required: true },
    shortUrl: { type: String, required: true },
    visitCount: { type: Number, default: 0 },
}, { timestamps: true })

export interface Url extends mongoose.Document {
    id: string,
    urlCode: String,
    longUrl: String,
    shortUrl: String,
    visitCount: number
}