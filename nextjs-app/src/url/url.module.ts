import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UrlController } from './url.controller';
import { urlSchema } from './url.model';
import { UrlService } from './url.service';


@Module({
    imports: [MongooseModule.forFeature([{ name: 'URL', schema: urlSchema }])],
    controllers: [UrlController],
    providers: [UrlService]
})

export class UrlModule { };