import { Controller, Post, Body } from '@nestjs/common';
import { generate } from 'shortid';
import { CreateUrlDto } from './url.dto';
import { UrlService } from './url.service';
const baseUrl = 'http://localhost:3000';

@Controller('url')
export class UrlController {
  constructor(private readonly UrlService: UrlService) {}
  @Post()
  async addUrl(@Body() body: CreateUrlDto) {
    const regex = new RegExp(
      '^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?',
    );
    const urlCode = generate();
    const shortUrl = body.desired_URL ? body.desired_URL : baseUrl + '/' + urlCode;
    const inUse = await this.UrlService.getOneUrl(body.longUrl);

    if (inUse.shortUrl) {
      return inUse.shortUrl;
    } else if (regex.test(body.longUrl)) {
      const newUrl = await this.UrlService.createUrl(
        urlCode,
        body.longUrl,
        shortUrl,
      );
      return newUrl.shortUrl;
    } else {
      return 'Please try another url';
    }
  }
}
