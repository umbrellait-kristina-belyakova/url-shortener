import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UrlModule } from './url/url.module';
import config from './config/keys';
import { urlSchema } from './url/url.model';

@Module({
  imports: [UrlModule, MongooseModule.forRoot(config.mongoURI), MongooseModule.forFeature([{ name: 'URL', schema: urlSchema }])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
