import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Url } from './url/url.model';

@Injectable()
export class AppService {
  constructor(@InjectModel("URL") private readonly UrlModel: Model<Url>) { }

  async getLongUrl(shortUrl: string): Promise<any> {
    try {
      const UrlFound = await this.UrlModel.findOne({shortUrl});
      UrlFound.visitCount = UrlFound.visitCount + 1;
      
      await UrlFound.save()
      return UrlFound.longUrl;

    } catch (error) {
      console.log(error);
      return error.message;
    }
  }
}
