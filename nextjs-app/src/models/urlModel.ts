import * as mongoose from 'mongoose'

const urlSchema = new mongoose.Schema({
    urlCode: { type: String, required: true },
    longUrl: { type: String, required: true },
    shortUrl: { type: String, required: true },
    date: {
        type: Date,
        default: Date.now
    }
})

const URL = mongoose.model('Url', urlSchema, 'Urls');

export default URL;